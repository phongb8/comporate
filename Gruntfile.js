'use strict';

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin',
        includereplace: 'grunt-include-replace'
    });

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    var modRewrite = require('connect-modrewrite');
    var serveStatic = require('serve-static');

    // Configurable paths
    var config = {
        app: 'app',
        dist: 'dist',
        assets: 'app/assets',
        distassets: 'dist/assets',
        bower: 'bowercomponent'
    };

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        config: config,

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            bower: {
                files: ['bower.json'],
                tasks: ['wiredep']
            },
            includereplace: {
                files: [
                    '<%= config.app %>/{,*/}*.html',
                    '<%= config.app %>/modules/{,*/}*.html',
                    '<%= config.app %>/modules/global/{,*/}*.html',
                    '<%= config.app %>/templates/{,*/}*.html',
                    '<%= config.assets %>/data/{,*/}*.json'
                ],
                tasks: ['includereplace:server']
            },
            js: {
                files: ['<%= config.app %>/**/*.js'],
                tasks: ['jshint'],
                options: {
                    livereload: '<%= connect.options.livereload %>'
                }
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            // compass: {
            //     files: ['<%= config.app %>/**/*.{scss,sass}', '<%= config.app %>/**/**/*.{scss,sass}'],
            //     tasks: ['compass:server', 'postcss:server']
            // },
            sass: {
                files: ['<%= config.app %>/**/*.{scss,sass}', '<%= config.app %>/**/**/*.{scss,sass}'],
                tasks: ['sass:server', 'postcss:server']
            },
            styles: {
                files: ['<%= config.app %>/**/*.css','<%= config.app %>/assets/styles/*.css'],
                tasks: ['newer:copy:server', 'postcss:server']
            },

            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>',
                    keepalive: true
                },
                files: [

                    '<%= config.app %>/{,*/}*.html',
                    '<%= config.app %>/module/{,*/}*.html',
                    '<%= config.app %>/module/global/{,*/}*.html',
                    '<%= config.app %>/templates/{,*/}*.html',
                    '<%= config.app %>/{,*/}*.tpl',
                    '.tmp/{,*/}*.html',
                    '<%= config.app %>/assets/scripts/{,*/}*.js',
                    '<%= config.app %>/assets/module/{,*/}*.js',
                    '<%= config.app %>/assets/template/{,*/}*.js',
                    '.tmp/assets/styles/{,*/}*.css',

                    '<%= config.assets %>/images/{,*/}*'
                ]
            }
        },

        // The actual grunt server settings
        connect: {
            options: {
                port: 9999,
                open: true,
                livereload: 32759,
                hostname: 'localhost' // Change this to '0.0.0.0' to access the server from outside
            },
            livereload: {
                options: {
                    middleware: function (connect) {
                        return [
                            modRewrite(['^[^\\.]*$ /index.html [L]']),
                            serveStatic('.tmp'),
                            connect().use('/bower_components', serveStatic('./bower_components')),
                            serveStatic(config.assets),
                            serveStatic(config.app)
                        ];
                    }
                }
            },
            dist: {
                options: {
                    base: '<%= config.dist %>',
                    livereload: false
                }
            }
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: ['dist']
                }]
            },
            server: {
                files: [{
                    dot: true,
                    src: ['.tmp']
                }]
            }
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish'),
            },
            all: [
                'Gruntfile.js',
                '<%= config.assets %>/scripts/{,*/}*.js',
                '<%= config.app %>/modules/{,*/}*.js',
                '<%= config.app %>/modules/global/{,*/}*.js',
                '<%= config.app %>/templates/{,*/}*.js',
                '!<%= config.assets %>/scripts/vendor/*'
            ]
        },
        // Compiles Sass to CSS and generates necessary files if requested
        // compass: {
        //     options: {
        //         cssDir: '.tmp/assets/styles',
        //         sassDir: '<%= config.assets %>/styles',
        //         imagesDir: '<%= config.assets %>/images',
        //         fontsDir: '<%= config.assets %>/fonts',
        //         generatedImagesDir: '<%= config.assets %>/images/generated',
        //         generatedImagesPath: '<%= config.assets %>/images/generated',
        //         javascriptsDir: '<%= config.assets %>/scripts',
        //         httpGeneratedImagesPath: '../images/generated',
        //         httpImagesPath: '../images',
        //         httpFontsPath: '../fonts',
        //         relativeAssets: false,
        //         assetCacheBuster: false,
        //         noLineComments: false
        //     },
        //     dist: {
        //         options: {
        //             generatedImagesDir: '<%= config.distassets %>/images/generated'
        //         }
        //     },
        //     server: {
        //         options: {
        //             debugInfo: false
        //         }
        //     }
        // },

        sass: {
            options: {
                outputStyle: 'compressed'
            },
            dist: {
                files: {
                    'dist/assets/styles/main.css': 'app/assets/styles/main.scss',
                    'dist/assets/styles/vendor.css': 'app/assets/styles/vendor.scss',
                    'dist/assets/styles/print.css': 'app/assets/styles/print.scss'
                }
            },
            server: {
                files: {
                    '.tmp/assets/styles/main.css': 'app/assets/styles/main.scss',
                    '.tmp/assets/styles/vendor.css': 'app/assets/styles/vendor.scss',
                    '.tmp/assets/styles/print.css': 'app/assets/styles/print.scss'
                }
            }
        },
        // Add vendor prefixed styles
        postcss: {
            options: {
                browsers: ['last 3 version'],
                processors: [
                    require('autoprefixer')({ browsers: ['last 3 versions'] })
                ]
            },

            server: {
                files: [{
                    expand: true,
                    cwd: '.tmp/assets/styles/',
                    src: '{,*/}*.css',
                    dest: '.tmp/assets/styles/'
                }]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%=config.distassets %>/styles/',
                    src: '{,*/}*.css',
                    dest: '<%=config.distassets %>/styles/'
                }]
            }
        },


        includereplace: {
            server: {
                options: {
                },
                files: [
                    { src: '*.html', dest: '.tmp/', expand: true, cwd: '<%= config.app %>/' },
                ]
            },
            dist: {
                options: {
                },
                files: [
                    { src: '*.html', dest: '<%= config.dist %>/', expand: true, cwd: '<%= config.app %>/' },
                ]
            }
        },
        // Automatically inject Bower components into the HTML file
        wiredep: {
            app: {
                src: [
                    '<%= config.app %>/head.tpl',
                    '<%= config.app %>/assets/styles/vendor.scss',
                    '<%= config.app %>/script.tpl'
                ]
            }
        },

        // Renames files for browser caching purposes
        filerev: {
            options: {
                algorithm: 'md5',
                length: 8
            },
            files: {
                src: [
                    '<%= config.dist %>/scripts/{,*/}*.js',
                    '<%= config.dist %>/styles/{,*/}*.css',
                    '<%= config.dist %>/images/{,*/}*.*',
                    '<%= config.dist %>/styles/fonts/{,*/}*.*',
                    '<%= config.dist %>/*.{ico,png}'
                ]
            }
        },
        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        useminPrepare: {
            options: {
                dest: '<%= config.dist %>'
            },
            html: [
                '<%= config.app %>/head.tpl',
                '<%= config.app %>/script.tpl'
            ]
        },
        // Performs rewrites based on rev and the useminPrepare configuration
        usemin: {
            options: {
                assetsDirs: [
                    '<%= config.dist %>',
                    '<%= config.distassets %>/images'
                ]
            },
            html: ['<%= config.dist %>/{,*/}*.html'],
            css: ['<%= config.dist %>/styles/{,*/}*.css', '<%= config.dist %>/styles/**/{,*/}*.css', '.tmp/asets/styles/**/{,*/}*.css']
        },
        // concat:{
        //     options: {
        //     }
        // },
        // uglify: {
        //     options: {
        //         preserveComments: false,
        //         compress: {
        //             drop_console: true
        //         }
        //     }
        // },
        // cssmin:{
        //     dist: {

        //     }
        // },
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.assets %>/images',
                    src: '{,*/}*.{gif,jpeg,jpg,png}',
                    dest: '<%= config.distassets %>/images'
                }, {
                        expand: true,
                        cwd: '<%= config.assets %>/data',
                        src: '{,*/}*.{gif,jpeg,jpg,png}',
                        dest: '<%= config.distassets %>/data'
                    }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.assets %>/images',
                    src: '{,*/}*.svg',
                    dest: '<%= config.distassets %>/images'
                },
                    {
                        expand: true,
                        cwd: '<%= config.assets %>/fonts',
                        src: '{,*/}*.svg',
                        dest: '<%= config.distassets %>/fonts'
                    }
                ]
            }
        },
        htmlmin: {
            dist: {
                options: {
                    collapseBooleanAttributes: true,
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    removeAttributeQuotes: true,
                    removeCommentsFromCDATA: true,
                    removeEmptyAttributes: true,
                    removeOptionalTags: true,
                    removeRedundantAttributes: true,
                    useShortDoctype: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= config.dist %>',
                    src: '{,*/}*.html',
                    dest: '<%= config.dist %>'
                }]
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            common: {
                files:
                [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.dist %>',
                    src: [
                        '*.{ico,png,txt, md}'
                    ]
                },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.assets %>/data/',
                        dest: '<%= config.distassets %>/data/',
                        src: '**'
                    },
                    {expand: true,dot: true,cwd: '<%= config.assets %>/libs/',dest: '<%= config.distassets %>/libs/',src: '**'},
                    {
                        expand: true,
                        dot: true,
                        cwd: '.tmp/assets/styles/',
                        dest: '<%= config.distassets %>/styles/',
                        src: ['*.css', '**/*.css']
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.assets %>/fonts/',
                        dest: '<%= config.distassets %>/fonts/',
                        src: '**'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.assets %>/scripts/',
                        dest: '<%= config.distassets %>/scripts/',
                        src: '**'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.assets %>/images/',
                        dest: '<%= config.distassets %>/images/',
                        src: '**'
                    }]
            },
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>/angularjs/',
                    dest: '<%= config.dist %>/angularjs/',
                    src: '**/*.tpl'
                }]
            },
            server: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '.tmp/',
                    src: [
                        'assets/styles/{,*/}*.css',
                        'assets/fonts/{,*/}*.*'
                    ]
                },
                    {
                        expand: true,
                        dot: true,
                        cwd: '.',
                        src: 'bower_components/bootstrap-sass/assets/fonts/bootstrap/*',
                        dest: '<%= config.dist %>'
                    }]
            }
        },

        // Run some tasks in parallel to speed up build process
        concurrent: {
            server: [
                'includereplace:server',
                //'compass:server',
                'sass:server',
                'copy:server'
            ],
            test: [
            ],
            dist: [
                //'compass',
                'sass:dist',
                'copy:common',
                'copy:dist',
                'imagemin',
                'svgmin'
            ]
        }
    });


    grunt.registerTask('serve', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'connect:dist:keepalive']);
        }
        if (target === 'preview') {
            return grunt.task.run(['connect:dist:keepalive']);
        }
        grunt.task.run([
            'clean:server',
            'concurrent:server',
            'postcss:server',
            'connect:livereload',
            'watch'
        ]);
    });

    grunt.registerTask('server', function (target) {
        grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
        grunt.task.run([target ? ('serve:' + target) : 'serve']);
    });

    grunt.registerTask('build', [
        'clean:dist',
        'wiredep',
        'concurrent:dist',
        'includereplace:dist',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin',
        'postcss:dist'
    ]);

    grunt.registerTask('clear', [
        'clean:server',
        'clean:dist'
    ]);

    grunt.registerTask('lint', [
        'jshint'
    ]);

    grunt.registerTask('default', [
        'serve'
    ]);

};