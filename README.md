# Introduction

Boilerplate and workflow for frontend development

## Features

* Automagically load grunt task - Just In Time (jit-grunt)
* CSS vendor prefixing with grunt-postcss
* Build-in preview server with LiveReload
* Automagically compile SCSS
* Automagically lint your scripts
* Automagically bundle css and js files
* SVG optimization
* Image optimization (via OptiPNG, pngquant, jpegtran)
* HTML partials with grunt-include-replace
* Static asset revisioning through file content hash with grunt-filerev

## Install

## Directory structures

```bash
.
├── Gruntfile.js
├── README.md
├── app
│   ├── assets
│   │   ├── data
│   │   │   └── data.json
│   │   ├── fonts
│   │   ├── images
│   │   │   ├── coffee.jpg
│   │   ├── scripts
│   │   │   └── main.js
│   │   └── styles
│   │       └── main.scss
│   ├── index.html
│   ├── modules
│   ├── head.tpl
│   ├── script.tpl
│   └── templates
│       └── home
│           ├── script.js
│           ├── style.scss
│           └── view.html
├── bower.json
└── package.json
```

## Run (Development)

```bash
$ grunt
```
or
```bash
$ grunt serve
```

### addtional grunt command

Inject bower package to head.html and script.html
Run command below after install an bower package (bower install jquery --save)
```bash
$ grunt bowerInstall
```

Clear .tmp and dist

```bash
$ grunt clear //clear .tmp and dist directory
$ grunt clean:server //clear .tmp, .sass-cache directory
$ grunt clean:dist //clear dist directory
```

## JSHint

Runs jslint on the javascript source.
```bash
$ grunt lint
```

## Build

```bash
$ grunt build
```

### Build and preview
```bash
$ grunt serve:dist
```

```bash
$ grunt serve:preview // preview dist only
```
