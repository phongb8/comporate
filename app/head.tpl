<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="fragment" content="!">
<meta name="viewport" content="width=device-width">
<link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
<meta name="format-detection" content="telephone=no">
 <link href="https://fonts.googleapis.com/css?family=Raleway:300,500,700" rel="stylesheet">

 <!-- build:css(.tmp) assets/styles/vendor.css -->
<link rel="stylesheet" href="assets/styles/vendor.css" />
<!-- bower:css -->
<link rel="stylesheet" href="../bower_components/slick-carousel/slick/slick.css" />
<link rel="stylesheet" href="../bower_components/jScrollPane/style/jquery.jscrollpane.css" />
<!-- endbower -->
<!-- endbuild -->

<!-- build:css(.tmp) assets/styles/main.css -->
<link rel="stylesheet" href="assets/styles/main.css" />
<!-- endbuild -->
<!-- build:css(.tmp) assets/styles/print.css -->
<link rel="stylesheet" href="assets/styles/print.css" media="print" />
<!-- endbuild -->
<!-- endstylepageout-->
<!-- endstylemoduleout-->
