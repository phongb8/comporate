$(function(){
	var bannerSlide = $('.banner-section');
	
	if(bannerSlide.length> 0){

		bannerSlide.slick({
			dots: true,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 6000
		});
		bannerSlide.on('afterChange', function(event, slick){
			
			var checkVideo = $('.slick-active .view-video').length;
			$('.view-video .tmp').remove();
			$('.view-video iframe').show();
			if(checkVideo>0){
				var html = '<iframe class="tmp" width="100%" height="100%" src="'+$('.slick-active .view-video iframe').attr('src')+'?autoplay=1&showinfo=0&loop=1" frameborder="0" allowfullscreen></iframe>';
				$('.slick-active .view-video iframe').hide();
				$('.slick-active .view-video').append(html);
			}
			checkVideo= 0;
			
			

		});
	}
})