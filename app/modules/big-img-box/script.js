$(function(){
	var content = $('.big-img-box .content');
	content.each(function(){
		var _this = $(this);
		_this.height(_this.height()).removeClass('fix-height')
				.find('.table').height(_this.height());
	})
	if($('.leader-style').length>0){
		content.find('a').click(function(){
			var _this = $(this);
			if(_this.closest('.content').hasClass('active')){
				_this.closest('.content').removeClass('active');
				_this.removeClass('active');
			}else{
				_this.closest('.content').addClass('active');
				_this.addClass('active');
			}
		});
	}
})