
(function ( $ ) {

	$.fn.convertHeight = function() {
		var element = $(this);
		$(element).height('auto');
		var h1 = 0;
		var itemss = $(element);
		itemss.each(function() {
			if( h1 < $(this).height() ) {
				h1 = $(this).height();
			}
		});
		itemss.each(function() {
			$(this).height(h1);

		});
		$(window).resize(function(){
			$(element).height('auto');
			var h1 = 0;
			var itemss = $(element);
			itemss.each(function() {
				if( h1 < $(this).height() ) {
					h1 = $(this).height();
				}
			});
			itemss.each(function() {
				$(this).height(h1);

			});
		})
	};
}( jQuery ));

$(function() {
	var k1,k2;
	$('.nav > li').hover(function() {
		if($(window).width()>767){
			var _this = this;
			$('.nav > li > ul').removeClass('show');
			$(_this).find('> ul').addClass('show');
			$(_this).find('> ul > li').convertHeight();
		}
		
		
	},function() {
		if($(window).width()>767){
			var _this = this;
			$(_this).find('> ul').removeClass('show');
		}
	});
	$('.nav li > a').click(function(){
		if($(window).width()<768){
			var _this = $(this);
			if(!_this.closest('li').hasClass('hover')){
				_this.closest('li').addClass('hover');
				return false;
			}
		}
	});
	$('.menu-button').click(function(){
		var _this = $(this);
		var $menucontainer = $('#menu-container');
		var $body = $('body');
		var $header = $('#header-section');
		var $html = $('html');
		if(_this.hasClass('active')){
			_this.removeClass('active');
			$menucontainer.removeClass('active');
			$('.nav li.hover').removeClass('hover');
			$body.removeClass('active');
			$header.removeClass('active');
			$html.removeClass('active');
		}else{
			_this.addClass('active');
			$menucontainer.addClass('active');
			$body.addClass('active');
			$html.addClass('active');
			$header.addClass('active');
		}
	});
	$(window).scroll(function(){
		if($(window).scrollTop()>100){
			$('#header-section').addClass('scrolling');
		}else{
			$('#header-section').removeClass('scrolling');
		}
	})
})
