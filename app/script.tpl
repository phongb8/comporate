 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBm8SAVI5QQfkmNNgsaoyR2eM8eLCYt8iI&callback=myMap"></script>
 <!-- build:js assets/scripts/vendor.js -->
<!-- bower:js -->
<script src="../bower_components/jquery/dist/jquery.js"></script>
<script src="../bower_components/bootstrap-sass/assets/javascripts/bootstrap.js"></script>
<script src="../bower_components/select2/dist/js/select2.js"></script>
<script src="../bower_components/slick-carousel/slick/slick.js"></script>
<script src="../bower_components/parallax.js/parallax.min.js"></script>
<script src="../bower_components/jScrollPane/script/jquery.mousewheel.js"></script>
<script src="../bower_components/jScrollPane/script/mwheelIntent.js"></script>
<script src="../bower_components/jScrollPane/script/jquery.jscrollpane.js"></script>
<script src="../bower_components/jScrollPane/script/jquery.jscrollpane.min.js"></script>
<script src="../bower_components/TweenMax.min/index.js"></script>
<script src="../bower_components/ScrollToPlugin.min/index.js"></script>
<!-- endbower -->
<!-- endbuild -->
<!-- build:js assets/scripts/main.js -->
<script src="assets/scripts/main.js"></script>
<script src="templates/communities/script.js"></script>
<script src="templates/denver-front-range/script.js"></script>
<script src="templates/about-giffis-residential/script.js"></script>
<script src="templates/why-live-with-us/script.js"></script>
<script src="templates/leadership/script.js"></script>
<script src="templates/green-living/script.js"></script>
<script src="templates/investors/script.js"></script>
<script src="templates/careers/script.js"></script>
<script src="templates/news/script.js"></script>
<script src="templates/loyalty-reward/script.js"></script>
<script src="templates/contact-us/script.js"></script>
<!-- endtemplate -->

<script src="modules/header/script.js"></script>
<script src="modules/footer/script.js"></script>
<script src="modules/banner/script.js"></script>
<script src="modules/premier-apartment-communities/script.js"></script>
<script src="modules/mapsection/script.js"></script>
<script src="modules/quality-experience-is-our-priority/script.js"></script>
<script src="modules/searchbar/script.js"></script>
<script src="modules/denver-front-range/script.js"></script>
<script src="modules/seattle-metro-wa/script.js"></script>
<script src="modules/las-vegas-nv/script.js"></script>
<script src="modules/austin-tx/script.js"></script>
<script src="modules/map-in-tab/script.js"></script>
<script src="modules/list-in-tab/script.js"></script>
<script src="modules/view-map-list/script.js"></script>
<script src="modules/text-over-img/script.js"></script>
 
<script src="modules/big-img-box3/script.js"></script>
<script src="modules/big-img-box/script.js"></script>
<script src="modules/big-img-box2/script.js"></script>
<script src="modules/your-views/script.js"></script>
<script src="modules/your-list/script.js"></script>
<script src="modules/leader2/script.js"></script>
<script src="modules/leader3/script.js"></script>
<script src="modules/leader1/script.js"></script>
<script src="modules/icon-list/script.js"></script>
<script src="modules/we-views/script.js"></script>
<script src="modules/investors-section2/script.js"></script>
<script src="modules/text-on-img/script.js"></script>
<script src="modules/text-on-img2/script.js"></script>
<script src="modules/img-list/script.js"></script>
<script src="modules/icon-list2/script.js"></script>
<script src="modules/news/script.js"></script>
<script src="modules/loyalty-rewards/script.js"></script>
<script src="modules/contact-map/script.js"></script>
<script src="modules/contact-content/script.js"></script>
<!-- endmodule -->

<!-- endbuild -->
<!-- endpageoutscripts -->
<!-- endmoduleoutscripts -->
